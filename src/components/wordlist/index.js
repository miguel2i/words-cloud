import { h, Component } from 'preact';
// import style from './style';
import WordListModel from './model';
import Word from './word';

export default class WordList extends Component {
	constructor () {
		super();
		this.model = new WordListModel(this.change);
		this.setState({ words: [], textword: '' });
		this.myname ='me';
		this.myname = prompt("Votre nom :");
	}

	change = () => {
		this.setState({ words: this.model.words });
	}

	addWord = () => {
		let word =  this.state.textword;
	
		if(word)
		{
			this.model.addWord(word, this.myname);
			this.state.textword = '';
		}
	}

	setText = e => {
		this.setState({ textword: e.target.value });
	}

	ontoggle = word => {
		this.model.toggle(word);
	}
	
	onweight = (word, weight) => {
		this.model.setWeight(word,weight);
	}
	
	render(props,  state) {
		return (
		<div>
		<form onSubmit={this.addWord} action="javascript:" >
				<span>{this.myname} : Ajouter un mot
				<input value={this.textword} onChange={this.setText} /></span>
				<button type="submit" >Add</button>
			</form>
			<ul>
				{ state.words.map( word =>
				<li><Word {...word} ontoggle={this.ontoggle} onweight={this.onweight} /></li>
				)}
			</ul>
			</div>
		);
	}
}

