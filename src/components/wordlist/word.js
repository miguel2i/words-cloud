import { h, Component } from 'preact';
import style from './style';

export default class Word extends Component {
    inc = () => {
	this.props.onweight(this, this.props.weight + 1);
    };

    dec = () => {
	let w = this.props.weight;
	this.props.onweight(this, w > 0 ? w-1 : 0);
    };

   render({name, weight, person, showperson, ontoggle, onweight}) {
            return ( 
		    <div>
                    <div onClick={() => ontoggle(this) }>
                    Name: { name } <br/>
                    Poids: { weight } </div>
		  <Personnes person={person} showperson={showperson} />
		  <button onClick={this.dec} >-</button>
		  <button onClick={this.inc} > + < /button> </div>
		  )
    };
}

function Personnes ({person, showperson}) {
	if(showperson )
		return (
				<div>
				personnes : <ul> { person.map(p => ( <li> {p} </li> )) } 
				</ul></div>
		       )
}

