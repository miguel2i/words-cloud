import WordListModel from '../src/components/wordlist/model'

let assert= require('assert');

describe('WordListModel', function() {
	let m;
	beforeEach(function(){
		m=new WordListModel(()=>{});
	});
	it('should init', function() {
		m.addWord('toto', 'me');
		assert.deepEqual([{ name: 'toto', weight: 1, person: ['me'] , showperson: false}]
			, m.words);
	});
	it('shoud return 1 size element',function(){
		m.addWord('toto');
		assert.equal(1, m.words.length);
	});
	it('shoud return 2 element', function() {
		m.addWord('titi', 'not me');
		m.addWord('toto', 'me');
		assert.equal(2, m.words.length);
	});
	it('when insert 2 same word, it shoud return 1 element with weight of 2', function() {
		m.addWord('toto', 'me');
		m.addWord('toto', 'not me');
		assert.equal(1, m.words.length);
		assert.equal(2, m.words[0].weight);
	});
	it('when inset 2 same word, it shoud return 1 element with weight of 2 (bis)',function() {
		m.addWord('a', 'one');
		m.addWord('b', 'two');
		m.addWord('a', 'two');
		assert.deepEqual([{ name: 'a', weight: 2, person: ['one', 'two'], showperson: false },
			{ name: 'b', weight: 1, person: ['two'], showperson: false }],
			m.words);
	});
	it('user can\'t add the same word', function() {
		m.addWord('a', 'toto');
		m.addWord('a', 'toto');
		assert.deepEqual(
			[{ name: 'a', weight: 1, person: ['toto'], showperson: false }],
			m.words);
	});

});
