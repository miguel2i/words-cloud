
export default class WordListModel {
	constructor (sub) {
		this.onchanges = [sub];
		this.words = [];
		this.wordset= {};
	}

	inform() {
		this.onchanges.forEach( cb => cb() );
	}

	normWord(w) {
		return w.toLowerCase();
	}

	addWord(word, person=null) {
		let n = this.normWord(word);
		let index = this.wordset[n];

		if (index === undefined) {
			this.words.push({ name: word, weight: 1, person: [person], showperson: false });
			this.wordset[n] = this.words.length -1 ;
			this.inform();
		}
		else {
			let i = this.words[index];
			if (!i.person.includes(person)) {
				i.person.push(person);
				i.weight += 1;
				this.inform();
			}
		}
	}

	setWeight(word, weight){
		let index = this.words.map(w => w.name).indexOf(word.props.name);
		if (this.words[index].weight !== weight) {
			this.words[index].weight = weight;
			this.inform();
		}
	}

	toggle(word) {
		this.words = this.words.map( w =>
			(w.name === word.props.name ?
				{ ...w, showperson: ! w.showperson } :	// toggle this word
				{ ...w, showperson: false })); 		//  hide other words
		this.inform();
	}
}

