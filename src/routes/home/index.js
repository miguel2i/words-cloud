import { h, Component } from 'preact';
import style from './style';
import WordList from '../../components/wordlist';
import Word from '../../components/wordlist/word';

export default class Home extends Component {
	render() {
		return (
				<div class={style.home}>
				<h1>Home</h1>
				<p>This is the Home component.</p>
				<WordList />
				</div>
		       );
	}
}

